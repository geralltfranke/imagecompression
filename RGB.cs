﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public struct RGB
{
    private byte _r;
    private byte _g;
    private byte _b;

    public RGB(byte r, byte g, byte b)
    {
        this._r = r;
        this._g = g;
        this._b = b;
    }

    public byte R
    {
        get { return this._r; }
        set { this._r = value; }
    }

    public byte G
    {
        get { return this._g; }
        set { this._g = value; }
    }

    public byte B
    {
        get { return this._b; }
        set { this._b = value; }
    }

    public bool Equals(RGB rgb)
    {
        return (this.R == rgb.R) && (this.G == rgb.G) && (this.B == rgb.B);
    }

    public static YUV ToYUV(RGB rgb)
    {
        double y = (double)((0.299 * (double)rgb.R) + (0.587 * (double)rgb.G) + (0.114 * (double)rgb.B));
        double u = (double)((-0.147 * (double)rgb.R) - (0.289 * (double)rgb.G) + (0.436 * (double)rgb.B));
        double v = (double)((0.615 * (double)rgb.R) - (0.515 * (double)rgb.G) - (0.100 * (double)rgb.B));

        return new YUV(y, u, v);
    }
}