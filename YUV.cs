﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public struct YUV
{
    private double _y;
    private double _u;
    private double _v;

    public YUV(double y, double u, double v)
    {
        this._y = y;
        this._u = u;
        this._v = v;
    }

    public double Y
    {
        get { return this._y; }
        set { this._y = value; }
    }

    public double U
    {
        get { return this._u; }
        set { this._u = value; }
    }

    public double V
    {
        get { return this._v; }
        set { this._v = value; }
    }

    public bool Equals(YUV yuv)
    {
        return (this.Y == yuv.Y) && (this.U == yuv.U) && (this.V == yuv.V);
    }

    public static RGB ToRGB(YUV yuv)
    {
        byte r = (byte)(yuv.Y + 1.4075 * (yuv.V - 128));
        byte g = (byte)(yuv.Y - 0.3455 * (yuv.U - 128) - (0.7169 * (yuv.V - 128)));
        byte b = (byte)(yuv.Y + 1.7790 * (yuv.U - 128));

        return new RGB(r, g, b);
    }
}