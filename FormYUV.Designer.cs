﻿namespace ImageCompression
{
    partial class FormYUV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picInputRGB = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoadInputImage = new System.Windows.Forms.Button();
            this.picY = new System.Windows.Forms.PictureBox();
            this.picU = new System.Windows.Forms.PictureBox();
            this.picV = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picInputRGB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picV)).BeginInit();
            this.SuspendLayout();
            // 
            // picInputRGB
            // 
            this.picInputRGB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picInputRGB.Location = new System.Drawing.Point(15, 35);
            this.picInputRGB.Name = "picInputRGB";
            this.picInputRGB.Size = new System.Drawing.Size(171, 177);
            this.picInputRGB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picInputRGB.TabIndex = 0;
            this.picInputRGB.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Input RGB Image";
            // 
            // btnLoadInputImage
            // 
            this.btnLoadInputImage.Location = new System.Drawing.Point(193, 35);
            this.btnLoadInputImage.Name = "btnLoadInputImage";
            this.btnLoadInputImage.Size = new System.Drawing.Size(171, 23);
            this.btnLoadInputImage.TabIndex = 2;
            this.btnLoadInputImage.Text = "Browse and convert to YUV";
            this.btnLoadInputImage.UseVisualStyleBackColor = true;
            this.btnLoadInputImage.Click += new System.EventHandler(this.BtnLoadInputImage_Click);
            // 
            // picY
            // 
            this.picY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picY.Location = new System.Drawing.Point(12, 261);
            this.picY.Name = "picY";
            this.picY.Size = new System.Drawing.Size(171, 177);
            this.picY.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picY.TabIndex = 3;
            this.picY.TabStop = false;
            // 
            // picU
            // 
            this.picU.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picU.Location = new System.Drawing.Point(193, 261);
            this.picU.Name = "picU";
            this.picU.Size = new System.Drawing.Size(171, 177);
            this.picU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picU.TabIndex = 4;
            this.picU.TabStop = false;
            // 
            // picV
            // 
            this.picV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picV.Location = new System.Drawing.Point(370, 261);
            this.picV.Name = "picV";
            this.picV.Size = new System.Drawing.Size(171, 177);
            this.picV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picV.TabIndex = 5;
            this.picV.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(190, 245);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "U";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(367, 245);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "V";
            // 
            // FormYUV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.picV);
            this.Controls.Add(this.picU);
            this.Controls.Add(this.picY);
            this.Controls.Add(this.btnLoadInputImage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.picInputRGB);
            this.Name = "FormYUV";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picInputRGB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picInputRGB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLoadInputImage;
        private System.Windows.Forms.PictureBox picY;
        private System.Windows.Forms.PictureBox picU;
        private System.Windows.Forms.PictureBox picV;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

