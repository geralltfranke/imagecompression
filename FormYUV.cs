﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageCompression
{
    public partial class FormYUV : Form
    {
        public FormYUV()
        {
            InitializeComponent();
        }

        private void BtnLoadInputImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.ShowDialog();

            Image inputRGB = Bitmap.FromFile(ofd.FileName);
            picInputRGB.Image = inputRGB;

            // Convert RGB image to YUV
            Bitmap y;
            YUV420.ConvertBitmapToYUV((Bitmap)inputRGB, out y);

            picY.Image = (Image)y;
        }
    }
}
